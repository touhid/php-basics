<?php

$x = true;
$y = false;

?>

<?php

$var = true;
$var = 1;

$var = false;
$var = 0;

if ( $var ) {
    echo 'var = true <br />';
}

if ( $var === true ) {
    echo 'var is a boolean and = true';
}

if ( !$var ) {
    echo 'var = false <br />';
}

if ( $var === false ) {
    echo 'var is a boolean and = false';
}

?>

<?php

$true_value = true;
$false_value = false;

print ("true_value = " . $true_value);
print ("false_value = " . $false_value);

?>